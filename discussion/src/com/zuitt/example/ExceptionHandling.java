package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args){
        //Exceptions
        //Compile-time - syntax errors
        //             - during compilation
        //Run-time - during execution

        Scanner scanner = new Scanner(System.in);
        int num = 0;
        System.out.println("Please enter number");

        try{
            num = scanner.nextInt();
        }catch(Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }finally {
            System.out.println("You have entered: " + num);
        }

        System.out.println("Hello Batch344");
    }
}