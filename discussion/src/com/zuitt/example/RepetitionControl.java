package com.zuitt.example;

public class RepetitionControl {
    public static void main(String[] args){
        // Loops
        //While
        int x=0;
        while(x<10){
            System.out.println("The number is " + x++);
        }

        //Do-While
        x=0;
        do{
            System.out.println("The number is " + x++);
        }while(x<10);

        //For
        for(int i=0; i<10; i++){
            System.out.println("The number is " + i);
        }

        String[] nameArray = {"John", "Paul", "George", "Ringo"};

        for(String name : nameArray) System.out.println(name);

        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for(String[] singleClass : classroom){
            for(String className : singleClass){
                System.out.println(className);
            }
        }
    }
}